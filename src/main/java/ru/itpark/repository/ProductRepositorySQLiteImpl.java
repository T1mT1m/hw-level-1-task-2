package ru.itpark.repository;

import ru.itpark.domain.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductRepositorySQLiteImpl implements ProductRepository {
    private String url;

    public ProductRepositorySQLiteImpl(String url) {
        this.url = url;
    }

    @Override
    public List<Product> findAll(){
        List<Product> products = new ArrayList<Product>();
        try(Connection connection = DriverManager.getConnection(url)){
            try(Statement statement = connection.createStatement()){
                ResultSet resultSet = statement.executeQuery("SELECT id, name, price, quantity FROM products");
                while (resultSet.next()){
                    products.add(new Product(
                            resultSet.getInt("id"),
                            resultSet.getString("name"),
                            resultSet.getInt("price"),
                            resultSet.getInt("quantity")
                    ));
                }


            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return products;
    }


    @Override
    public void add(Product product){
        try(Connection connection = DriverManager.getConnection(url)){
            try(PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO product id, name, price, quantity VALUES (?,?,?,?)")){
                statement.setInt(1, product.getId());
                statement.setString(2, product.getName());
                statement.setInt(3, product.getId());
                statement.setInt(4, product.getQuantity());
                statement.executeUpdate();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Product product){
        try(Connection connection = DriverManager.getConnection(url)){
            try(PreparedStatement statement = connection.prepareStatement(
                    "UPDATE product SET  id = ?, name = ?, price = ?, quantity = ?")){
                statement.setInt(1, product.getId());
                statement.setString(1, product.getName());
                statement.setInt(1, product.getPrice());
                statement.setInt(1, product.getQuantity());

                statement.executeUpdate();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Product> removeId(int id){
        try(Connection connection = DriverManager.getConnection(url)){
            try(PreparedStatement statement = connection.prepareStatement("DELETE FROM products WHERE id = ?")){
                statement.setInt(1, id);

                statement.executeUpdate();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Product> sortByAZ() {
        return null;
    }

    @Override
    public List<Product> sortByZA() {
        return null;
    }

    @Override
    public List<Product> sortMaxPrice() {
        return null;
    }

    @Override
    public List<Product> sortMinPrce() {
        return null;
    }

}
