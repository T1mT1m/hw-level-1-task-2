package ru.itpark.repository;

import ru.itpark.domain.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> findAll();

    void add(Product product);

    void update(Product product);

    List<Product> removeId(int id);

    List<Product> sortByAZ();

    List <Product> sortByZA();

    List<Product> sortMaxPrice();

    List<Product> sortMinPrce();
}
