package ru.itpark.repository;

import ru.itpark.domain.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryInMemory implements ProductRepository {

    private List<Product> products = new ArrayList<>();

    @Override
    public List<Product> findAll() {
        List<Product> list= new ArrayList<>();
        return list;
    }

    @Override
    public void add(Product product) {
        products.add(product);

    }

    @Override
    public void update(Product product) {
        for (Product product1 : products) {

        }

    }

    @Override
    public List<Product> removeId(int id) {
        products.removeIf(product -> product.getId() == id);

        return products;
    }
    @Override
    public List<Product> sortByAZ(){
        products.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
        return products;
    }

    @Override
    public List <Product> sortByZA(){
        products.sort((o1, o2) -> o2.getName().compareTo(o1.getName()));
        return products;
    }

    @Override
    public List<Product> sortMaxPrice(){
        products.sort((o1, o2) -> o2.getPrice() - o1.getPrice());
        return products;
    }
    @Override
    public List<Product> sortMinPrce(){
        products.sort((o1, o2) -> o1.getPrice() - o2.getPrice());
        return products;
    }
}
