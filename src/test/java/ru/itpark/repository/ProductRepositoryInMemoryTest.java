package ru.itpark.repository;

import jdk.nashorn.api.tree.NewTree;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.itpark.domain.Product;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductRepositoryInMemoryTest {
    private ProductRepositoryInMemory test;

    @BeforeEach
    void setUp() {
        test = new ProductRepositoryInMemory();
    }

    @Test
    void findAll() {

        List<Product> list = test.findAll();
        assertEquals(0, test.findAll().size());
    }


    @Test
    @DisplayName("Add goods")
    void add() {
        test.add(new Product(1, "PowerBank", 5000, 7));
        test.add(new Product(2, "MacBook", 80_000, 4));
        test.add(new Product(3, "Asus", 35_000, 7));
        List<Product> list = test.findAll();
        assertEquals(3, test.findAll().size());

    }

    @Test
    @DisplayName("Update list")
    void update() {
        test.add(new Product(1, "PowerBank", 5000, 7));
        test.add(new Product(2, "MacBook", 80_000, 4));
        test.add(new Product(3, "Asus", 35_000, 7));
        test.add(new Product(4, "HP", 35_000, 10));
        List<Product> list = test.findAll();
        assertEquals(4, list.size());
    }

    @Test
    void removeId() {
        test.add(new Product(1, "PowerBank", 5000, 7));
        test.add(new Product(2, "MacBook", 80_000, 4));
        test.add(new Product(3, "Asus", 35_000, 7));
        test.add(new Product(4, "HP", 35_000, 10));
        test.removeId(1);

        List<Product> list = test.findAll();


        assertEquals(0, list.size());

    }

    @Test
    void sortByAZ() {
        test.add(new Product(1, "PowerBank", 5000, 7));
        test.add(new Product(2, "MacBook", 80_000, 4));
        test.add(new Product(3, "Asus", 35_000, 7));
        test.sortByAZ();
        List<Product> list = test.sortByAZ();

        assertEquals("MacBook", list.get(1).getName());

    }

    @Test
    void sortByZA() {
    }

    @Test
    void sortMaxPrice() {
    }

    @Test
    void sortMinPrice() {
    }
}