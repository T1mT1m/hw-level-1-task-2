package ru.itpark.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class ProductRepositorySQLiteImplTest {
    private String url = "jdbc:sqlite:db.sqlite6";
    private ProductRepository productRepository;

    @BeforeEach
    void setUp() throws SQLException {
        try(Connection connection = DriverManager.getConnection(url)){
            try(Statement statement = connection.createStatement()){
                statement.execute("DROP TABLE IF EXISTS goods");
                statement.execute("CREATE TABLE goods(\n" +
                        "  id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                        "  name TEXT NOT NULL,\n" +
                        "  price INTEGER NOT NULL CHECK (price > 0),\n" +
                        "  quantity INTEGER NOT NULL CHECK (quantity >= 0) DEFAULT 0\n" +
                        "\n" +
                        ")");

            }

        }

        productRepository = new ProductRepositorySQLiteImpl(url);

    }

    @org.junit.jupiter.api.Test
    @DisplayName("findAll")
    void findAll() {
    }

    @org.junit.jupiter.api.Test
    void add() {
    }

    @org.junit.jupiter.api.Test
    void update() {
    }

    @org.junit.jupiter.api.Test
    void removeId() {
    }
}